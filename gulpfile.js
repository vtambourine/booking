var path = require('path');
var fork = require('child_process').fork;

var gulp = require('gulp');
var gutil = require('gulp-util');
var changed = require('gulp-changed');

var webpack = require('webpack');
var bundler = webpack(require('./webpack.config.js'));
var WebpackDevServer = require('webpack-dev-server')
var webpackClientConfig = require('./webpack.config')[0];

function bundleHandler(error, stats) {
    if (error) {
        throw new gutil.PluginError('webpack', error);
    }
    gutil.log('Webpack bundle ready:', stats.toJson().assetsByChunkName);
}

gulp.task('default', ['build']);

gulp.task('assets', function () {
    const ASSETS = [
        'src/page*/*.*'
    ];

    gulp.src(ASSETS)
        .pipe(changed('build'))
        .pipe(gulp.dest('build'));
});

gulp.task('build', ['assets'], function () {
    bundler.run(bundleHandler);
});

gulp.task('watch', function (done) {
    //gulp.watch('src/page*/*.*', ['assets']);

    const SERVER = 'build/server.js';

    //bundler.watch(300, bundleHandler);

    new WebpackDevServer(webpack(webpackClientConfig), {
        publicPath: 'http://localhost:3000/',
        hot: true,
        historyApiFallback: true,
        contentBase: path.resolve(__dirname, '/')
    }).listen(3000, 'localhost', function (error, result) {
        if (error) {
            console.log(error);
        }

        var started = false;
        var server = (function startup() {
            var child = fork(SERVER);
            //child.once('message', function(message) {
            //    if (message === 'online') {
            //        if (!started) {
            //            started = true;
            //            gulp.watch(SERVER, function () {
            //                gutil.log('Restarting server');
            //                server.kill('SIGTERM');
            //                server = startup();
            //            });
            //            done();
            //        }
            //    }
            //});
            return child;
        })();

        console.log('Listening at http:/localhost:3000');
    });

    process.on('exit', function () {
        server.kill('SIGTERM');
    });
});

gulp.task('webpack-dev-server', ['assets'], function (done) {
    new WebpackDevServer(webpack(webpackClientConfig), {
        publicPath: path.resolve(__dirname, '/'),
        hot: true,
        historyApiFallback: true,
        inline: true
    }).listen(3000, 'localhost', function (error, result) {
        if (error) {
            console.log(error);
        }
        console.log('Listening at http:/localhost:5000');
    });
});
