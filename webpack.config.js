var path = require('path');
var extend = require('extend');
var webpack = require('webpack');

var config = {
    output: {
        path: path.join(__dirname, 'build'),
    },
    resolve: {
        extensions: ['', '.js']
    },
  };

var applicationConfig = extend(true, {}, config, {
    devtools: 'eval',
    entry: [
        'webpack-dev-server/client?http://localhost:3000',
        'webpack/hot/only-dev-server',
        './src/application.js'
    ],
    output: {
        filename: 'application.js',
        publicPath: 'http://localhost:3000/'
    },
    resolve: {
        root: [
            path.resolve('./src/core'),
            path.resolve('./src/actions'),
            path.resolve('./src/stores'),
            path.resolve('./src/constants'),
            path.resolve('./src/components')
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loaders: ['react-hot', 'babel-loader']
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.less$/,
                loader: 'style-loader!css-loader!less-loader'
            },
            {
                test: /\.woff\d?(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&minetype=application/font-woff"
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&minetype=application/octet-stream"
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file"
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&minetype=image/svg+xml"
            }

        ]
    }
});

var serverConfig = extend(true, {}, config, {
    entry: './src/server.js',
    output: {
        filename: 'server.js',
        libraryTarget: 'commonjs2'
    },
    externals: /^[a-z][a-z\.\-0-9]*$/,
    target: 'node',
    node: {
        console: false,
        global: false,
        process: false,
        Buffer: false,
        __filename: false,
        __dirname: false
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                include: path.join(__dirname, 'src'),
                loader: 'babel-loader'
            }
        ]
    }
});

module.exports = [applicationConfig, serverConfig];
