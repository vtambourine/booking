import fs from 'fs';
import path from 'path';
import express from 'express';

var server = express();

var PORT = process.env.PORT || 5000;
server.use(express.static(path.join(__dirname)));

var template = fs.readFileSync(path.join(__dirname, 'page/index.html'), 'utf8');

server.get('/', (request, response) => {
    response.setHeader('Content-Type', 'text/html');
    response.end(template);
});

server.listen(PORT, () => {
    if (process.send) {
        process.send('online');
    } else {
        console.log(`Application is running at port ${PORT}`);
    }
});
