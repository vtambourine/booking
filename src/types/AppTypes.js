import {PropTypes} from 'react';

export var AppTypes = {
    cargo: function() {
        var validator = PropTypes.shape({
            type: PropTypes.oneOf(['type']).isRequired,
            label: PropTypes.string,
            placeholder: PropTypes.string,
            value: PropTypes.string
        });
        var validationError = validator.apply(null, arguments);
        if (validationError) {
            throw validationError;
        }
    },

    field: function() {
        var validator = PropTypes.shape({
            type: PropTypes.oneOf(['type']).isRequired,
            label: PropTypes.string,
            placeholder: PropTypes.string,
            value: PropTypes.string
        });
        var validationError = validator.apply(null, arguments);
        if (validationError) {
            throw validationError;
        }
    }
};
