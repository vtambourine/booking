import React from 'react';
import AppDispatcher from '../../core/AppDispatcher';
import CargoStore from './CargoStore';
import CargoActionCreators from './CargoActionCreators';

class CargoComponent extends React.Component {
    state = {title: this.props.name};

    constructor(props) {
        super(props);
        this.store = new CargoStore(this, {name: this.props.name});
        this.actions = new CargoActionCreators(this);
    }

    componentDidMount() {
        this.store.addChangeListener(this._onChange);
    }

    componentWillUnmount () {
        this.store.removeChangeListener(this._onChange);
        this.store.destruct();
    }

    render() {
        return (
            <p>
                <strong onClick={this._onClick}>1 - {this.state.title}</strong>
                {this.state.isEditing ? 'LALALAL' : ''}
            </p>
        );
    }

    _onChange = () => {
        //console.log('cargo item changed');
        this.setState({
            title: this.store.getName(),
            isEditing: this.store.isEditing()
        })
    }

    _onClick = () => {
        //console.log('cargo clicked');
        this.actions.editCargo();
    }
}

class Cargo {
    constructor(props) {
        this.component = React.createElement(CargoComponent, props);
    }

    getComponent() {
        return this.component;
    }
}

export default Cargo;
