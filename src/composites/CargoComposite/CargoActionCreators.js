import AppDispatcher from '../../core/AppDispatcher.js';
import {ActionTypes} from '../../constants/AppConstants';

class CargoActionCreators {
    constructor(cargo) {
        this.cargo = cargo;
        this.handleCargoAction = AppDispatcher.dispatchCargoAction.bind(AppDispatcher, this.cargo);
    }

    editCargo() {
        this.handleCargoAction({
            action: ActionTypes.EDIT_CARGO
        });
    }
}

export default CargoActionCreators;
