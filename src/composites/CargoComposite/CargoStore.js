import {EventEmitter} from 'events';
import {ActionTypes} from '../../constants/AppConstants';
import AppDispatcher from '../../core/AppDispatcher';

const CHANGE_EVENT = 'change';

var _isEditing = false;
var _name;

class CargoStore extends EventEmitter {
    constructor(cargo, props) {
        super();
        this.cargo = cargo;
        this.name = props.name;
        console.log(this.name)
        this._isEditing = false;
        this._handlePayload = this._handlePayload.bind(this)
        this.dispatchToken = AppDispatcher.register(this._handlePayload)
    }

    isEditing() {
        return this._isEditing;
    }

    getName() {
        return this.name;
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    destruct() {
        AppDispatcher.unregister(this._handlePayload);
    }

    _handlePayload = (payload) => {
        if (payload.source === this.cargo) {
            switch (payload.action.action) {
                case ActionTypes.EDIT_CARGO:
                    this._isEditing = true;
                    this.emitChange();
            }
        }
    }
}

export default CargoStore;
