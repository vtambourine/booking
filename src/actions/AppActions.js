import AppDispatcher from '../core/AppDispatcher';
import ActionTypes from '../constants/ActionTypes.js';

var AppActions = {
    createDelivery(delivery) {
        AppDispatcher.dispatch({
            action: ActionTypes.CREATE_DELIVERY,
            delivery
        });
    },

    createCargo(cargo) {
        AppDispatcher.dispatch({
            action: ActionTypes.ADD_CARGO,
            cargo
        });
    },

    updateCargo(id, updates) {
        AppDispatcher.dispatch({
            action: ActionTypes.UPDATE_CARGO,
            id,
            updates
        });
    }
}

export default AppActions;
