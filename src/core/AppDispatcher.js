import { Dispatcher } from 'flux';

class AppDispatcher extends Dispatcher {
    dispatchCargoAction(cargo, action) {
        this.dispatch({
            source: cargo,
            action
        });
    }
}

export default new AppDispatcher();
