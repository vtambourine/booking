import { EventEmitter } from 'events';
import AppDispatcher from '../core/AppDispatcher';
import DeliveryStore from '../stores/DeliveryStore';
import {ActionTypes} from '../constants/AppConstants';
import React from 'react';

const CHANGE_EVENT = 'change';

class Store extends EventEmitter {
    name = 'none';

    constructor(cargo) {
        super();
        this.cargo = cargo;
    }

    setName(name) {
        this.name = name;
    }

    getName(name) {
        return this.name
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }
}

class Actions {
    constructor(cargo, name) {
        this.cargo = cargo;
        this.name = name;
    }


    editCargo() {
        AppDispatcher.dispatch({
            action: ActionTypes.EDIT_CARGO,
            name: this.name,
            cargo: this.cargo
        });
    }

    updateCargo(value) {
        console.log('update')
        AppDispatcher.dispatch({
            action: ActionTypes.UPDATE_CARGO,
            name: value,
            cargo: this.cargo
        });
    }
}

class Cargo extends React.Component {
    constructor(props) {
        super(props);
        this.store = new Store(this);
        this.actions = new Actions(this, this.props.name);

        AppDispatcher.register(this.handlePayload);
    }

    getActionCreator() {
        return this.actions;
    }

    state = {title: this.props.name};

    componentDidMount() {
        this.store.addChangeListener(this._onChange);
    }

    componentWillUnmount () {
        this.store.removeChangeListener(this._onChange);

        AppDispatcher.unregister(this.handlePayload);
    }

    render() {
        return (
            <p><strong onClick={this.handleClick}>1 - {this.state.title}</strong></p>
        );
    }

    handleClick = () => {
        this.actions.editCargo(this);
    }

    handlePayload = (payload) => {
        if (payload.action === ActionTypes.UPDATE_CARGO && payload.cargo === this) {
            console.log('cargo actions', this.store, payload.name);
            this.store.setName(payload.name);
            this.store.emitChange();
        }
    }

    _onChange = () => {
        console.log('cargo item changed')
        this.setState({
            title: this.store.getName()
        })
    }
}

var CargoComponentFactory = {
    _create(name) {
        var cargoComp = React.createElement(Cargo, {name});
        return cargoComp;
    },

    create(name) {

    }
};

export default CargoComponentFactory;
