class Cargo {
    constructor({id, name, ...fields}) {
        this.id = id;
        this.name = name;
        this.fields = fields.reduce((result, field) => {
            result[field] = {
                type: 'text',
                placeholder: field
            };
            return result;
        }, {});
    }

    update(updates) {
        for (let key of updates) {
            this.fields[key] = updates[key];
        }
    }

    serialize() {
        var result = '';
        result += `${this.id}: ${this.name}\n===\n`;
        for (let f in this.fields) {
            result += `${f}: ${this.fields[f]}\n`;
        }
        return result;
    }

}

export default Cargo;
