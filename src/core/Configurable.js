class Configurable {
    _options = [];

    constructor(fields) {
        for (let field in fields) {
            this._options.push({
                type: 'text',
                name: field
            });
        }
    }
}

export default Configurable;
