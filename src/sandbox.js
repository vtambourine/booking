class Store {
    constructor(name) {
        this.name = name;
    }

    getName() {
        return this.name;
    }
}

class Cargo {
    constructor(name) {
        this.store = new Store(name);
    }
}

var Factory = {
    create(name) {
        return class CargoComponent {
            constructor() {
                this.component = new Cargo(name);
                this.store = new Store(name);
            }

            getStore() {
                console.log('get store', this.store.getName())
            }

            getComponent() {
                console.log('get component')
            }
        }
    }
}

var C = Factory.create('LOL');
var c = new C('Baba');

c.getStore();
c.getComponent();
