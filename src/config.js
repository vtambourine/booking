var cargoTypes = {
    table: {
        name: 'Стол',
        fields: ['Количество']
    },
    chair: {
        name: 'Стул',
        fields: ['Количество']
    },
    car: {
        name: 'Автомобили',
        fields: ['Марка', 'Количество']
    },
    motorcycle: {
        name: 'Мототехника',
        fields: ['Марка', 'Количество']
    },
    shortDistanceTravel: {
        name: 'Перевозки по городу',
        fields: ['Количество']
    },
    longDistanceTravel: {
        name: 'Междугородние перевозки',
        fields: ['Количество']
    },
    sand: {
        name: 'Песок',
        fields: ['Вес']
    },
    chipStone: {
        name: 'Щебень',
        fields: ['Вес']
    },
    oil: {
        name: 'Нефтепродукты',
        fields: ['Вес']
    },
    grain: {
        name: 'Зерно',
        fields: ['Вес']
    }
};

var deliveryTypes = [
    {
        id: 'household',
        name: 'Домашние вещи',
        cargos: [cargoTypes.table, cargoTypes.chair]
    },
    {
        id: 'moves',
        name: 'Переезды',
        cargos: [cargoTypes.table, cargoTypes.chair, cargoTypes.car, cargoTypes.motorcycle]
    },
    {
        id: 'vehicles',
        name: 'Транспорт',
        cargos: [cargoTypes.car, cargoTypes.motorcycle]
    },
    {
        id: 'passengers',
        name: 'Пассажиры',
        cargos: [cargoTypes.shortDistanceTravel, cargoTypes.longDistanceTravel]
    },
    {
        id: 'granular',
        name: 'Насыпной груз',
        cargos: [cargoTypes.sand, cargoTypes.chipStone, cargoTypes.grain]
    },
    {
        id: 'effuse',
        name: 'Наливной груз',
        cargos: [cargoTypes.oil]
    },
    {
        id: 'agriculture',
        name: 'Сельхозпродукты',
        cargos: [cargoTypes.grain]
    }
];

export {deliveryTypes, cargoTypes}
