import React from 'react';

var Dialog = React.createClass({
    //state = {
    //    type: this.props.text,
    //    fields: this.props.fields
    //}

    componentDidMount() {
        $('#myModal')
            .on('hide.bs.modal', () => {
                if (this.props.onClose) {
                    this.props.onClose();
                }
            })
            .modal('show');
    },

    render() {
        return (
            <div className='Dialog'>
                <div className="modal"
                     ref={this._onMount}
                     tabIndex="-1"
                     role="dialog"
                     aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button className="close"
                                        type="button"
                                        data-dismiss="modal"
                                        aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 className="modal-title" id="myModalLabel">Modal title</h4>
                            </div>
                            <div className="modal-body">
                                <input type='text' onChange={this._onChange} value={this.state.type}/>
                                {this.props.content}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                <button
                                    type="button"
                                    className="btn btn-primary"
                                    onClick={this._onSave}>Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    },

    _onMount = (component) => {
        console.log('dialog mounted');
        console.log(React.findDOMNode(component))
    },

    _onChange = (event) => {
        this.setState({
            type: event.target.value
        });
    },

    //_onFieldChange = (field, event) => {
    //    this.setState({
    //        fields: Object.assign({}, this.state.fields, {
    //            [field]: {
    //                value: event.target.value
    //            }
    //        })
    //    })
    //}

    _onSaveClick = (event) => {
        if (this.props.onSave) {
            this.props.onSave(this.state.fields);
        }
        if (this.props.onClose) {
            this.props.onClose();
        }
    }
});

export default Dialog;
