import React, {PropTypes} from 'react';
import AppActions from '../../actions/AppActions';
import CargoDialog from '../CargoDialog';
import {AppTypes} from '../../types/AppTypes';

import './CargoListItem.less';

var CargoListItem = React.createClass({

    propTypes: {
        type: PropTypes.string.isRequired,
        fields: PropTypes.objectOf(AppTypes.field)
    },

    getInitialState() {
        return {
            isEditing: false
        }
    },

    _onClick() {
        this.setState({isEditing: true});
    },

    _onDialogClose() {
        this.setState({isEditing: false});
    },

    _onDialogSave(updates) {
        AppActions.updateCargo(this.props.id, updates);
    },

    render() {
        var itemProps = (
            <dl className="CargoListItem-properties">
                {Object.keys(this.props.fields).map((name) => {
                    return (
                        <div key={name}>
                            <dt className="CargoListItem-property-name">{name}</dt>
                            <dd className="CargoListItem-property-value">{this.props.fields[name].value}</dd>
                        </div>
                     );
                })}
            </dl>
        );

        var item = (
            <div className='CargoListItem' onClick={this._onClick}>
                <div className="CargoListItem-edit">
                    <span className="CargoListItem-icon" />
                </div>
                <div className="CargoListItem-remove">
                    <span className="CargoListItem-icon" />
                </div>
                <h4>{this.props.type}</h4>
                {itemProps}
            </div>
        );

        var dialog = this.state.isEditing ?
            <CargoDialog
                key={this.props.key}
                fields={this.props.fields}
                onClose={this._onDialogClose}
                onSave={this._onDialogSave} /> :
            null;

        return (
            <div>
                {item}
                {dialog}
            </div>
        );
    }

});


export default CargoListItem;
