import React, {PropTypes} from 'react';
import CargoListItem from '../CargoListItem';
import {AppTypes} from '../../types/AppTypes';

import './CargoList.less';

var CargoList = React.createClass({

    propTypes: {
        cargos: PropTypes.objectOf(AppTypes.cargo)
    },

    render() {
        var cargoList = Object.keys(this.props.cargos).map((key) => {
            var cargo = this.props.cargos[key];
            return <CargoListItem key={key} {...cargo} />;
        });

        return (
            <div className="CargoList">
                <h3 className="CargoList-title">Грузы:</h3>
                {cargoList}
            </div>
        );
    }

});

export default CargoList;
