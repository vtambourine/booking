import React from 'react';
import AppActions from '../../actions/AppActions';

import './CargoType.less';

class CargoType extends React.Component {
    render() {
        return (
            <div className='CargoType'
                onClick={this._onClick}>
                <div className='CargoType-icon' />
                {this.props.cargo.name}
            </div>
        );
    }

    _onClick = () => {
        AppActions.createCargo(this.props.cargo);
    }
}

export default CargoType;
