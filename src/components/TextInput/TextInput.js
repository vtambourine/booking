import React from 'react';

const ENTER_KEY_CODE = 13;

class TextInput extends React.Component {
    //propTypes = {
    //    placeholder: ReactPropTypes.string,
    //    onSave: ReactPropTypes.func.isRequired,
    //    value: ReactPropTypes.string
    //}

    state = {
        value: this.props.value || ''
    };

    render() {
        return (
            <div className='TextInput'>
                <strong>{this.props.label}</strong>
                <input
                    type='text'
                    placeholder={this.props.placeholder}
                    value={this.state.value}
                    onBlur={this._onBlur}
                    onChange={this._onChange}
                    onKeyDown={this._onKeyDown} />
            </div>
        )
    }

    _onBlur = () => {
        this._save();
    }

    _onChange = (event) => {
        this.setState({
            value: event.target.value
        });
    }

    _onKeyDown = (event) => {
        if (event.keyCode === ENTER_KEY_CODE) {
            this._save();
        }
    }

    _save = () => {
        this.props.onSave(this.state.value);
    };
}

export default TextInput;
