import React from 'react';
import CargoType from '../CargoType';

class Subcategories extends React.Component {
    render() {
        return (
            <div className='Subcategories'>
                <h3>Категория: {this.props.delivery.name}</h3>
                <h4>Добавьте груз:</h4>
                {this.props.delivery.cargos.map((cargo, i) => {
                    return <CargoType
                        cargo={cargo}
                        key={i} />
                })}
            </div>
        );
    }
}

export default Subcategories;
