import React from 'react';
import Modal from 'controls/Modal';
import Field from '../Field';
import AppActions from '../../actions/AppActions';

class CargoDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        for (let key in this.props.fields) {
            this.state[key] = this.props.fields[key].value;
        }
    }

    render() {
        var content = [];

        for (let key in this.props.fields) {
            content.push(
                <Field key={key}
                       onSave={this._onSave.bind(this, key)}
                       {...this.props.fields[key]} />
            );
        }

        return (
            <div className='CargoDialog'>
                <Modal
                    title="Редактирование груза"
                    content={content}
                    onSave={this._onDialogSave}
                    onClose={this._onDialogClose} />
            </div>
        )
    }

    _onSave(key, value) {
        this.setState({
            [key]: {value}
        });
    }

    _onDialogClose = () => {
        this.props.onClose();
    };

    _onDialogSave = () => {
        this.props.onSave(this.state);
    };
}

export default CargoDialog;
