import React, {PropTypes} from 'react';

import './Modal.less';

var Modal = React.createClass({

    propTypes: {
        title: PropTypes.string.isRequired,
        content: PropTypes.node,
        saveText: PropTypes.string,
        closeText: PropTypes.string,
        onSave: PropTypes.func,
        onClose: PropTypes.func
    },

    getDefaultProps() {
        return {
            saveText: 'Сохранить',
            closeText: 'Закрыть',
            onSave() {},
            onClose() {}
        }
    },

    _onMount(component) {
        this._modalElement = $(React.findDOMNode(component));
        this._modalElement
            .on('hide.bs.modal', this._onClose)
            .modal('show');
    },

    _onSave() {
        this.props.onSave();
        this._modalElement.modal('hide');
    },

    _onClose() {
        this.props.onClose();
    },

    render() {
        return (
            <div className="Modal"
                 ref={this._onMount}
                 tabIndex="-1"
                 role="dialog">
                <div className="Modal-dialog">
                    <div className="Modal-content">
                        <div className="Modal-header">
                            <button className="close"
                                type="button"
                                data-dismiss="modal">
                                <span>&times;</span>
                            </button>
                            <h4 className="Modal-title">{this.props.title}</h4>
                        </div>
                        <div className="Modal-body">
                            {this.props.content}
                        </div>
                        <div className="Modal-footer">
                            <button className="Modal-button"
                                    type="button"
                                    data-dismiss="modal">
                                {this.props.closeText}
                            </button>
                            <button className="Modal-button"
                                    type="button"
                                    onClick={this._onSave}>
                                {this.props.saveText}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default Modal;
