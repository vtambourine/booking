import React from 'react';

import Header from 'Header';
import Footer from 'Footer';
import Categories from 'Categories';
import Subcategories from 'Subcategories';
import CargoList from 'CargoList';

import DeliveryStore from 'DeliveryStore';

import {deliveryTypes} from '../../config';

import './App.less';

function getState() {
    return {
        delivery: deliveryTypes[1],
        cargos: DeliveryStore.getCargos()
    }
}

var App = React.createClass({
    getInitialState() {
        return getState();
    },

    componentDidMount() {
        DeliveryStore.addChangeListener(this._onChange);
    },

    componentWillUnmount() {
        DeliveryStore.removeChangeListener(this._onChange);
    },

    _onChange() {
        this.setState(getState());
    },

    render() {
        var content;
        if (this.state.delivery) {
            content = (
                <div className="App-content">
                    <Subcategories delivery={this.state.delivery} />
                    <CargoList cargos={this.state.cargos} />
                </div>
            );
        } else {
            content = <Categories />;
        }

        return (
            <div className="App">
                <Header />
                {content}
                <Footer />
            </div>
        );
    }
});

export default App;
