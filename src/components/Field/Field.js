import React from 'react';
import TextInput from '../TextInput';

class Field extends React.Component {
    render() {
        var ControlClass = TextInput;
        return (
            <div className='Field'>
                <ControlClass {...this.props} />
            </div>
        )
    }
}

export default Field;
