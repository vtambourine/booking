import React from 'react';

import './Header.less';

class Header extends React.Component {
    render() {
        return (
            <div className="Header">
                <div className="Header-logo">
                    <h1>Везет Всем</h1>
                </div>
                <div className="Header-slogan">
                    <h3>Найдем лучшего перевозчика</h3>
                </div>
            </div>
        );
    }
}

export default Header;
