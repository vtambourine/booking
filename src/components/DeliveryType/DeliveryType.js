import React from 'react';
import AppActions from '../../actions/AppActions';

import './DeliveryType.less';

class DeliveryType extends React.Component {
    render() {
        return (
            <div className='DeliveryType'
                onClick={this._onClick}>
                <div className='DeliveryType-icon' />
                {this.props.delivery.name}
            </div>
        );
    }

    _onClick = () => {
        AppActions.createDelivery(this.props.delivery);
    }
}

export default DeliveryType;
