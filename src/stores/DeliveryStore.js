import {EventEmitter} from 'events';
import {update} from 'react/addons';
import ActionTypes from '../constants/ActionTypes';
import AppDispatcher from '../core/AppDispatcher';

import Cargo from '../core/Cargo';

const CHANGE_EVENT = 'change';

var _delivery;
var _cargos = {};

function createCargo(cargo) {
    var id = (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
    _cargos[id] = {
        id,
        type: cargo.name,
        fields: cargo.fields.reduce((result, name) => {
            result[name] = {
                type: 'text',
                label: name,
                placeholder: 'Dummy placeholder',
                value: ''
            };
            return result
        }, {})
    };
}

function updateCargo(id, updates) {
    for (let key in updates) {
        _cargos[id].fields[key] = Object.assign({}, _cargos[id].fields[key], updates[key]);
    }
}

class DeliveryStore extends EventEmitter {
    getDelivery() {
        return _delivery;
    }

    getCargos() {
        return _cargos;
    }

    getEditingCargo() {
        return _editingCargo;
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    dispatchToken = AppDispatcher.register((payload) => {
        switch (payload.action) {
            case ActionTypes.CREATE_DELIVERY:
                _delivery = payload.delivery;
                this.emitChange();
                break;
            case ActionTypes.ADD_CARGO:
                createCargo(payload.cargo);
                this.emitChange();
                break;
            case ActionTypes.UPDATE_CARGO:
                updateCargo(payload.id, payload.updates);
                this.emitChange();
                break;
        }
    });
}

export default new DeliveryStore();
