import keyMirror from 'react/lib/keyMirror';

export default keyMirror({
    CREATE_DELIVERY: null,
    ADD_CARGO: null,
    EDIT_CARGO: null,
    UPDATE_CARGO: null
});
